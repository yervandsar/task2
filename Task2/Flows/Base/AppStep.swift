//
//  AppStep.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxFlow

enum AppStep: Step {
    case main
}

enum HomeStep: Step {
    case weather(context: CityContext)
}

enum SearchStep: Step {
    case list
    case weather(context: CityContext)
}

enum TabItemStep: Int, Step, CaseIterable {
    case home
    case search

    var id: String {
        switch self {
        case .home:
            return "home"
        case .search:
            return "search"
        }
    }

    var title: String {
        switch self {
        case .home:
            return "Home"
        case .search:
            return "Search"
        }
    }

    var icon: UIImage? {
        return UIImage(named: self.id)
    }
}
