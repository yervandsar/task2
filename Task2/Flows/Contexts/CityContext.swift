//
//  CountryContext.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import CoreLocation

struct CityContext {
    let cityName: String?
    var mustDetectLocation: Bool {
        return cityName == nil
    }

    init(cityName: String? = nil) {
        self.cityName = cityName
    }
}
