//
//  MainFlow.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxFlow
import Swinject
import SwinjectAutoregistration

enum MainTabs: String, CaseIterable {

    case home = "Home"
    case settings = "Settings"

    var image: UIImage? {
        return nil
    }

}

final class MainFlow: Flow {

    var root: Presentable {
        return self.rootViewController
    }

    private let rootViewController: UITabBarController = {
        let tabBarController = UITabBarController()
        tabBarController.tabBar.barTintColor = UIColor.white
        tabBarController.tabBar.tintColor = .black
        tabBarController.tabBar.unselectedItemTintColor = .lightGray
        tabBarController.tabBar.isTranslucent = false
        tabBarController.tabBar.backgroundImage = UIImage()
        tabBarController.tabBar.shadowImage = UIImage()
        tabBarController.tabBar.clipsToBounds = true
        return tabBarController
    } ()

    private let parentAssembler: Assembler?
    private let assembler: Assembler
    private let assemblies: [Assembly] = []

    init(parentAssembler: Assembler? = nil) {
        self.parentAssembler = parentAssembler
        self.assembler = Assembler(assemblies, parent: parentAssembler)
    }

    func navigate(to step: Step) -> FlowContributors {
        if let step = step as? AppStep {
            return navigate(to: step)
        } else {
            return .none
        }
    }

    private func navigate(to step: AppStep) -> FlowContributors {
        switch step {
        case .main:
            return navigateToMainScreen()
        }
    }

    private func navigateToMainScreen() -> FlowContributors {
        let homeFlow = HomeFlow(parentAssembler: parentAssembler)
        let searchFlow = SearchFlow(parentAssembler: parentAssembler)

        Flows.whenReady (
            flow1: homeFlow,
            flow2: searchFlow
        ) { [unowned self] home, search in
            let vcs = [home, search]
            TabItemStep.allCases.enumerated().forEach { index, tab in
                let tabBarItem = UITabBarItem(title: tab.title, image: tab.icon, selectedImage: nil)
                vcs[index].tabBarItem = tabBarItem
            }
            self.rootViewController.setViewControllers(vcs, animated: false)

        }

        return .multiple(flowContributors: [
            .contribute(
                withNextPresentable: homeFlow,
                withNextStepper: OneStepper(withSingleStep: HomeStep.weather(context: CityContext()))),
            .contribute(
                withNextPresentable: searchFlow,
                withNextStepper: OneStepper(withSingleStep: SearchStep.list))
            ])
    }
}
