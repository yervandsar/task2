//
//  AppFlow.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxFlow
import Swinject
import SwinjectAutoregistration

final class AppFlow: Flow {

    var root: Presentable {
        return rootWindow
    }

    private let rootWindow: UIWindow
    private let parentAssembler: Assembler?
    private(set) var assembler: Assembler!
    var assemblies: [Assembly] {
        return []
    }

    init(withWindow window: UIWindow, parentAssembler: Assembler? = nil) {
        rootWindow = window
        self.parentAssembler = parentAssembler
        assembler = Assembler(assemblies, parent: parentAssembler)
    }

    func navigate(to step: Step) -> FlowContributors {
        if let step = step as? AppStep {
            return navigate(to: step)
        } else {
            return .none
        }
    }

    func navigate(to step: AppStep) -> FlowContributors {
        switch step {
        case .main:
            return navigateToMainScreen()
        }
    }

    private func navigateToMainScreen() -> FlowContributors {
        let mainFlow = MainFlow(parentAssembler: parentAssembler)
        Flows.whenReady(flow1: mainFlow) { [unowned self] root in
            self.rootWindow.rootViewController = root
        }
        return .one(flowContributor: .contribute(withNextPresentable: mainFlow,
                                                 withNextStepper: OneStepper(withSingleStep: AppStep.main)))
    }

}
