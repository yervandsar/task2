//
//  HomeFlow.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxFlow
import Swinject
import SwinjectAutoregistration

final class HomeFlow: HasFlow {

    var root: Presentable {
        return rootViewController
    }

    var rootViewController: UIViewController = UINavigationController()

    var assembler: Assembler

    var assemblies: [Assembly] = [
        HomeAssembly()
    ]

    var parentAssembler: Assembler?

    required init(parentAssembler: Assembler? = nil) {
        self.parentAssembler = parentAssembler
        assembler = Assembler(assemblies, parent: parentAssembler)
    }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? HomeStep else {
            return .none
        }
        switch step {
        case .weather(context: let context):
            return push(WeatherViewController.self) { $0.context = context }
        }
    }
}
