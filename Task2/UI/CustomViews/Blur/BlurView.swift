//
//  BlurView.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/2/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Reusable

final class BlurView: UIView, NibLoadable {

    @IBOutlet weak var settingsBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        settingsBtn
            .rx.tap
            .map { URL(string: UIApplication.openSettingsURLString) }
            .filterNil()
            .filter { UIApplication.shared.canOpenURL($0) }
            .subscribe(onNext: { url in
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            })
            .disposed(by: rx.disposeBag)
    }
}
