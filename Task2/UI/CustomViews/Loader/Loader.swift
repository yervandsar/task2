//
//  Loader.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/2/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Reusable

final class Loader: UIView, NibLoadable {

    @IBOutlet weak var primaryView: UIView! {
        didSet {
            primaryView.backgroundColor = .orange
            primaryView.layer.cornerRadius = 45
        }
    }
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var loaderView: UIView!

    let pulse = LoaderPulse(radius: 150)

    // MARK: - Pulse Animation for Request
    func startPulse() {
        pulse.position = CGPoint(x: 45, y: 45)
        pulse.animationDuration = 2
        pulse.backgroundColor = UIColor.orange.cgColor
        logoView.layer.insertSublayer(pulse, below: logoView.layer)
        UIView.animate(withDuration: 0.3) { [unowned self] in
            self.loaderView.alpha = 1
        }
    }

    func stopPulse(completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            DispatchQueue.main.async { [weak self] in
                self?.loaderView.alpha = 0
            }
            }, completion: { _ in
                completion()
        })
    }

}
