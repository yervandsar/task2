//
//  WeatherViewController.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxFlow
import RxCocoa
import CoreLocation
import SDWebImage

final class WeatherViewController: BaseViewController {

    override var stepper: Stepper {
        return viewModel
    }

    var viewModel: WeatherViewModel!
    var context: CityContext!

    @IBOutlet weak var unitSegmentControl: UISegmentedControl!
    @IBOutlet weak var tempLbl: UILabel!
    @IBOutlet weak var weatherInfoLbl: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var addressLbl: UILabel!

    private let locationManager = CLLocationManager()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return context.mustDetectLocation ? .lightContent : .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        showLoading()
        if let city = context.cityName {
            viewModel.getWeatherByCityName.onNext(city)
        } else {
            initLocationManager()
        }
        doBindings()
    }

    override func initNavigationBar() {
        navigationItem.title = "Weather App"
        navigationController?.setNavigationBarHidden(context.mustDetectLocation, animated: true)
    }

    private func initLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        updateLocation(status: CLLocationManager.authorizationStatus())
        NotificationCenter.default
            .rx.notification(UIApplication.didBecomeActiveNotification)
            .map { _ in CLLocationManager.authorizationStatus() }
            .subscribe(onNext: { [weak self] status in
                self?.updateLocation(status: status)
            })
            .disposed(by: rx.disposeBag)
    }

    private func doBindings() {
        viewModel.locationInfo
            .filterNil()
            .subscribe(onNext: { [weak self] info in
                guard let self = self else { return }
                self.weatherIcon.sd_setImage(with: info.weatherImageUrl)
                self.weatherInfoLbl.text = info.weatherStatus
                self.tempLbl.text = info.temp
                self.addressLbl.text = info.city + ", " + info.country
                self.hideLoading()
            })
            .disposed(by: rx.disposeBag)

        unitSegmentControl
            .rx.selectedSegmentIndex
            .map { WeatherUnit(rawValue: $0) }
            .filterNil()
            .observeOn(MainScheduler.instance)
            .bind(to: viewModel.changeUnit)
            .disposed(by: rx.disposeBag)
    }

    // MARK: - Loading
    private lazy var loadingView: Loader = {
        let loading = Loader.loadFromNib()
        loading.frame = self.view.frame
        return loading
    } ()

    func showLoading() {
        view.addSubview(loadingView)
        loadingView.startPulse()
    }

    func hideLoading() {
        loadingView.stopPulse { [weak self] in
            self?.loadingView.removeFromSuperview()
        }
    }

    // MARK: - Blur
    private lazy var blurView: BlurView = {
        let blur = BlurView.loadFromNib()
        blur.frame = self.view.frame
        return blur
    } ()

    func showPermissionFail() {
        view.addSubview(blurView)
    }

    func hidePermissionFail() {
        blurView.removeFromSuperview()
    }
}

extension WeatherViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let coordinates = locations.first?.coordinate else { return }
        viewModel.getWeatherByCoordinates.onNext(coordinates)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        updateLocation(status: status)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // this is required to implement but there is nothing to do here
    }

    func updateLocation(status: CLAuthorizationStatus) {
        switch status {
        case .denied, .restricted:
            showPermissionFail()
        case .authorizedWhenInUse, .authorizedAlways:
            hidePermissionFail()
            locationManager.requestLocation()
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
}
