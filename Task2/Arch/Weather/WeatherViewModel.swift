//
//  WeatherViewModel.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxFlow
import CoreLocation

final class WeatherViewModel: Stepper, HasDisposeBag {
    let steps = PublishRelay<Step>()

    let getWeatherByCoordinates = PublishSubject<CLLocationCoordinate2D?>()
    let getWeatherByCityName = PublishSubject<String?>()

    let changeUnit = BehaviorRelay<WeatherUnit>(value: .metric)

    let locationInfo = BehaviorRelay<LocationInfo?>(value: nil)

    init(service: WeatherServiceProtocol) {

        let coordinatesRequest = getWeatherByCoordinates
            .filterNil()
            .withLatestFrom(changeUnit) { WeatherQuery(coordinates: $0, unit: $1) }
        let cityRequest = getWeatherByCityName
            .filterNil()
            .withLatestFrom(changeUnit) { WeatherQuery(city: $0, unit: $1) }

        let unitChanged = changeUnit.share()

        let coordinatesWithUnit = unitChanged
            .withLatestFrom(getWeatherByCoordinates) { weatherUnit, coordinates -> WeatherQuery? in
                guard let coordinates = coordinates else { return nil }
                return WeatherQuery(coordinates: coordinates, unit: weatherUnit)
            }
            .filterNil()

        let cityNameWithUnit = unitChanged
            .withLatestFrom(getWeatherByCityName) { weatherUnit, cityName -> WeatherQuery? in
                guard let name = cityName else { return nil }
                return WeatherQuery(city: name, unit: weatherUnit)
            }
            .filterNil()

        Observable.merge(coordinatesRequest, cityRequest, coordinatesWithUnit, cityNameWithUnit)
            .flatMap { service.getWeather(query: $0) }
            .map { $0.response }
            .filterNil()
            .withLatestFrom(changeUnit) { LocationInfo(with: $0, and: $1) }
            .observeOn(MainScheduler.instance)
            .bind(to: locationInfo)
            .disposed(by: disposeBag)
    }
}
