//
//  CitiesViewModel.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxFlow
import GoogleMaps
import GooglePlaces

final class CitiesViewModel: Stepper, HasDisposeBag {
    let steps = PublishRelay<Step>()

    let searchTrigger = PublishSubject<String>()
    let openWeather = PublishSubject<String>()

    let searchResults = BehaviorSubject<[GMSAutocompletePrediction]>(value: [])

    init(service: CityServiceProtocol) {
        searchTrigger
            .distinctUntilChanged()
            .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
            .flatMap { service.searchCity(by: $0) }
            .subscribe(onNext: { [weak self] result in
                self?.searchResults.onNext(result)
            }, onError: { error in
                    dump(error.localizedDescription)
            })
            .disposed(by: disposeBag)

        openWeather
            .map(CityContext.init)
            .map(SearchStep.weather)
            .bind(to: steps)
            .disposed(by: disposeBag)
    }
}
