//
//  CitiesViewController.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxFlow
import RxCocoa
import GoogleMaps
import GooglePlaces

final class CitiesViewController: BaseViewController {

    override var stepper: Stepper {
        return viewModel
    }

    var viewModel: CitiesViewModel!

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    @IBOutlet weak var tableView: UITableView!

    lazy var searchController: UISearchController = {
        let search = UISearchController(searchResultsController: nil)
        search.obscuresBackgroundDuringPresentation = false
        return search
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.delegate = self
        doBindings()
        initTableView()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.searchController.dismiss(animated: true, completion: nil)
    }

    override func initNavigationBar() {
        super.initNavigationBar()
        navigationItem.title = "Search City"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }

    private func doBindings() {
        searchController
            .searchBar
            .rx.text
            .orEmpty
            .bind(to: viewModel.searchTrigger)
            .disposed(by: rx.disposeBag)
    }

    private func initTableView() {
        viewModel
            .searchResults
            .bind(to: tableView.rx.items(cellIdentifier: "cityCell", cellType: UITableViewCell.self)) { (_, element, cell) in
                cell.selectionStyle = .none
                cell.textLabel?.attributedText = element.attributedPrimaryText
            }
            .disposed(by: rx.disposeBag)

        tableView
            .rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let self = self else { return }
                self.tableView.deselectRow(at: indexPath, animated: false)
            })
            .disposed(by: rx.disposeBag)

        tableView.rx
            .setDelegate(self)
            .disposed(by: rx.disposeBag)

        tableView
            .rx.modelSelected(GMSAutocompletePrediction.self)
            .map { $0.attributedPrimaryText.string }
            .bind(to: viewModel.openWeather)
            .disposed(by: rx.disposeBag)
    }
}

extension CitiesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension CitiesViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        defer {
            searchController.dismiss(animated: true, completion: nil)
        }
        return true
    }
}
