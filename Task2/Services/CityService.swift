//
//  CityService.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import RxSwift
import GoogleMaps
import GooglePlaces

protocol CityServiceProtocol {
    func searchCity(by text: String) -> Observable<[GMSAutocompletePrediction]>
}

final class CityService: BaseService, CityServiceProtocol {

    func searchCity(by text: String) -> Observable<[GMSAutocompletePrediction]> {
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        return Observable.create { observer in
            GMSPlacesClient.shared().autocompleteQuery(
                text,
                bounds: nil,
                filter: filter,
                callback: { results, error in
                    if let error = error {
                        observer.onError(error)
                    } else {
                        observer.onNext(results ?? [])
                        observer.onCompleted()
                    }
            })
            return Disposables.create()
        }
    }
}
