//
//  WeatherService.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxSwift

protocol WeatherServiceProtocol {
    func getWeather(query: WeatherQuery) -> Observable<SingleState<WeatherResponse>>
}

final class WeatherService: BaseService, WeatherServiceProtocol {
    func getWeather(query: WeatherQuery) -> Observable<SingleState<WeatherResponse>> {
        return client.get("/weather", query: query)
    }
}
