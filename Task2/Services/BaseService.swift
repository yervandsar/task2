//
//  BaseService.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import Alamofire
import RxRestClient

class BaseService {
    let client: RxRestClient

    init(client: RxRestClient) {
        self.client = client
    }
}
