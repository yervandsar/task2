//
//  SearchAssembly.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import RxRestClient
import Alamofire

final class SearchAssembly: Assembly {
    func assemble(container: Container) {
        container.autoregister(CitiesViewModel.self, initializer: CitiesViewModel.init)
        container.register(CitiesViewController.self) { r in
            let controller = CitiesViewController.instantiate()
            controller.viewModel = r ~> CitiesViewModel.self
            return controller
        }
    }
}
