//
//  HomeAssembly.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import RxRestClient
import Alamofire

final class HomeAssembly: Assembly {
    func assemble(container: Container) {
        container.autoregister(WeatherViewModel.self, initializer: WeatherViewModel.init)
        container.register(WeatherViewController.self) { r in
            let controller = WeatherViewController.instantiate()
            controller.viewModel = r ~> WeatherViewModel.self
            return controller
        }
    }
}
