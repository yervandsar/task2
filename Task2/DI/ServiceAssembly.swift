//
//  ServiceAssembly.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import RxRestClient
import Alamofire

/// Inject services to container
final class ServiceAssembly: Assembly {
    func assemble(container: Container) {
        container.autoregister(WeatherServiceProtocol.self, initializer: WeatherService.init)
        container.autoregister(SessionManagerAdapter.self, initializer: SessionManagerAdapter.init)
        container.autoregister(SessionManager.self, initializer: provideSessionManager)
        container.autoregister(CityServiceProtocol.self, initializer: CityService.init)
        container.autoregister(RxRestClient.self, initializer: provideRestClient)
    }

    private func provideRestClient(_ manager: SessionManager) -> RxRestClient {
        var options = RxRestClientOptions.default

        options.logger = DebugRxRestClientLogger()
        options.jsonDecoder = JSONDecoder()
        options.jsonEncoder = JSONEncoder()
        options.sessionManager = manager
        options.urlEncoding = URLEncoding(boolEncoding: .literal)

        return RxRestClient(baseUrl: URL(string: Config.weatherUrl), options: options)
    }

    private func provideSessionManager(_ adapter: SessionManagerAdapter) -> SessionManager {
        let manager = SessionManager.default

        manager.adapter = adapter

        return manager
    }
}

final class SessionManagerAdapter: RequestAdapter {
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        return urlRequest
    }
}
