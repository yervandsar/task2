//
//  WeatherResponse.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation

struct WeatherResponse: Decodable {
    let weather: [WeatherData]
    let main: WeatherMainData
    let name: String
    let sys: WeatherSys
}

struct WeatherData: Decodable {
    let main: String
    let icon: String
    let description: String
}

struct WeatherMainData: Decodable {
    let temp: Double
    let pressure: Double
    let humidity: Double
    let temp_min: Double
    let temp_max: Double
}

struct WeatherSys: Decodable {
    let country: String
    let sunrise: Int
    let sunset: Int
}
