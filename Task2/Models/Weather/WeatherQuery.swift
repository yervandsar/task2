//
//  WeatherQuery.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import CoreLocation

struct WeatherQuery: Encodable {
    let lat: Double?
    let lon: Double?
    let q: String?
    let appid: String
    let units: String

    init(coordinates: CLLocationCoordinate2D, unit: WeatherUnit) {
        lat = coordinates.latitude
        lon = coordinates.longitude
        appid = Config.weatherAppKey
        units = unit.id
        q = nil
    }

    init(city: String, unit: WeatherUnit) {
        lat = nil
        lon = nil
        appid = Config.weatherAppKey
        units = unit.id
        q = city
    }
}
