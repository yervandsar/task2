//
//  LocationInfo.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation

struct LocationInfo: Codable {
    let city: String
    let country: String
    let temp: String
    let weatherStatus: String
    let weatherImageUrl: URL?

    init?(with response: WeatherResponse, and unit: WeatherUnit) {
        self.city = response.name
        self.country = Locale.current.localizedString(forRegionCode: response.sys.country) ?? response.sys.country
        self.temp = "\(response.main.temp) \(unit.symbol)"
        guard let weather = response.weather.first else {
            return nil
        }
        self.weatherStatus = weather.main
        self.weatherImageUrl = URL(string: "http://openweathermap.org/img/wn/\(weather.icon)@2x.png")
    }

}
