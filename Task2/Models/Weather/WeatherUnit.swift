//
//  WeatherUnit.swift
//  Task2
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation

enum WeatherUnit: Int, Encodable {
    case metric
    case imperial
    case standard

    var id: String {
        switch self {
        case .metric:
            return "metric"
        case .imperial:
            return "imperial"
        case .standard:
            return "standard"
        }
    }

    var symbol: String {
        switch self {
        case .metric:
            return "°C"
        case .imperial:
            return "°F"
        case .standard:
            return "K"
        }
    }
}
